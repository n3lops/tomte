# Welcome to TOMTE - Third-order matched transition events

... for the simulation of high-energy particle collisions.

## What am I doing here?

TOMTE is a new method that allows to combine next-to-next-to-next-to-leading-order QCD calculations with parton shower event generators, check out

https://inspirehep.net/literature/1867290

for details. This projects offers a prototype implementation providing all the necessary ingredients. We hope this'll get you started with implementing your own N3LO+PS process!

## Usage
The prototype implementation is not yet primed for easy usage, the goal of the code maily being to provide a baseline implementation from which you can find all nitty-gritty details that are otherwise hard to extract from publications. We hope it lives up to that promise! You can get started by consulting the WORKFLOW file.

### Generating input events for toy calculations

Hints to appear soon...

<!-- blank line -->

<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="http://home.thep.lu.se/~prestel/fysikum.png">
    <source src="http://home.thep.lu.se/~prestel/lecture3.mp4" type="video/mp4">
  </video>
</figure>

<!-- blank line -->


### Producing toy calculations

Hints to appear soon...

### Producing matched results

Hints to appear soon...

## Project members
Current project members are Stefan Prestel (stefan.prestel@thep.lu.se) and Valerio Bertone (valerio.bertone@cern.ch).

## License
This project licenced under the GNU GPL v2 or later.

## Project plans
The project aims to provide a prototype -- so depending on interest, progress might be fast (high interest), or slow (no interest). Let's see ;)
