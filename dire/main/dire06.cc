
// DIRE includes.
#include "Dire/Dire.h"

// Pythia includes.
#include "Pythia8/Pythia.h"

using namespace Pythia8;

int main(){
  Pythia pythia;
  Dire dire;
  dire.init(pythia, "lep.cmnd");
  return 0;
}
