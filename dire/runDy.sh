
exc0=n3lo-dy0p-exc/n3lo-dy0p-exclusive-sample.lhef
exc1=n3lo-dy1p-exc/n3lo-dy1p-exclusive-sample.lhef
exc2=n3lo-dy2p-exc/n3lo-dy2p-exclusive-sample.lhef
inc3=n3lo-dy3p-inc/n3lo-dy3p-inclusive-sample.lhef
neve=$1
seed=$2

name=n3lo-dy-matched
dir=n3lo-dy-matched
logd=n3lo-dy-matched
mkdir -p $dir
mkdir -p $logd
output=$dir/${name}.lhef
log=$logd/${name}.log

rm $log

begin=$(date -u +%s)

tag=$(date -u +%s)

tmp0=$logd/0p-$tag.tmp
tmp1=$logd/1p-$tag.tmp
tmp2=$logd/2p-$tag.tmp
tmp3=$logd/3p-$tag.tmp
cat cards/n3lo-dy0p.cmnd | grep -iv 'Beams:LHEF' > $tmp0
echo "Beams:LHEF = $exc0" >> $tmp0
cat cards/n3lo-dy1p.cmnd | grep -iv 'Beams:LHEF' > $tmp1
echo "Beams:LHEF = $exc1" >> $tmp1
cat cards/n3lo-dy2p.cmnd | grep -iv 'Beams:LHEF' > $tmp2
echo "Beams:LHEF = $exc2" >> $tmp2
cat cards/n3lo-dy3p.cmnd | grep -iv 'Beams:LHEF' > $tmp3
echo "Beams:LHEF = $inc3" >> $tmp3

./bin/dire --input $tmp0 --input $tmp1 --input $tmp2 --input $tmp3 --nevents $neve --setting "next:numbercount = 100" --setting "random:setseed = on" --setting "random:seed = $seed" --lhef_output $output | tee -a $log

rm $tmp0 $tmp1 $tmp2 $tmp3

echo ""
echo ""
echo ""

end=$(date -u +%s)

echo "run time $((end-begin))"

