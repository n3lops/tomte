
njet=$1

njetp1=$((njet+1))

input1=../MG5_aMC_v3_2_0/dd2ee_${njet}g/Events/dy${njet}g101/unweighted_events.lhe
input2=n3lo-dy${njetp1}p-inc/n3lo-dy${njetp1}p-inclusive-sample.lhef

outputd=n3lo-dy${njet}p-exc/
mkdir -p $outputd
output=${outputd}/n3lo-dy${njet}p-exclusive-sample.lhef

logd=n3lo-dy${njet}p-exc/
mkdir -p $logd
log=${logd}/n3lo-dy${njet}p-exclusive-sample.log

tag=$(date -u +%s)
tmp1=${logd}/makeToy-${njet}-$tag.tmp
tmp2=${logd}/makeToy-${njetp1}-$tag.tmp
cp cards/n3lo-create-toy-dy${njet}p-sample-inc.cmnd.template $tmp1
cp cards/n3lo-create-toy-dy${njet}p-sample-rec.cmnd.template $tmp2

sed "s,<lhef>,$input1,g" -i $tmp1
sed "s,<lhef>,$input2,g" -i $tmp2

rm $log

neve=$2
seed=$3

begin=$(date -u +%s)
./bin/dire --input $tmp1 --input $tmp2 --nevents $neve --setting "next:numbercount = 100" --setting "random:setseed = on" --setting "random:seed = $seed" --lhef_output $output | tee -a $log
end=$(date -u +%s)

rm $tmp1 $tmp2

echo "Finished successfully after $((end-begin)) seconds" | tee -a $log

