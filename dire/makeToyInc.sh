
njet=$1

input=../MG5_aMC_v3_2_0/dd2ee_${njet}g/Events/dy${njet}g101/unweighted_events.lhe
gunzip $input.gz

outputd=n3lo-dy${njet}p-inc/
mkdir -p $outputd
output=${outputd}/n3lo-dy${njet}p-inclusive-sample.lhef

logd=n3lo-dy${njet}p-inc/
mkdir -p $logd
log=${logd}/n3lo-dy${njet}p-inclusive-sample.log

tag=$(date -u +%s)
tmp=${logd}/makeToy-${njet}-$tag.tmp
cp cards/n3lo-create-toy-dy${njet}p-sample-inc.cmnd.template $tmp

sed "s,<lhef>,$input,g" -i $tmp

rm $log

neve=$2
seed=$3

begin=$(date -u +%s)

./bin/dire --input $tmp --nevents $neve --setting "next:numbercount = 100" --setting "random:setseed = on" --setting "random:seed = $seed" --lhef_output $output | tee -a $log
end=$(date -u +%s)

rm $tmp

echo "Finished successfully after $((end-begin)) seconds" | tee -a $log

