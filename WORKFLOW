## Installation:
# The Tomte code relies on Pythia, Dire, LHAPDF and Apfel.
# These may be installed by executing recompile.sh
# Note that this script will download and install LHAPDF
# automatically.

chmod +x recompile.sh
./recompile.sh

# Note on resources:
# The figures in arxiv:2202.xxxxx have been produced from
# a) 50M MG5 events for each multiplicity
# b) which were used to produce 5M toy calculation events for each
#    multiplicity,
# c) which then were then used as input for Tomte.

# Generation of inputs for the construction of toy DY calculations:
# Enter MG5_aMC_v3_2_0 and execute makeDYTOYLHEFs.sh
# Note that by changing the MG5 process cards procs_dd2ee*g.dat, you
# may generate inputs for toy calculations of other processes.
# Note: The number of events requested from MG5 should be a 
# factor of ~10-15 larger than the number of events required from
# the toy calculations.

cd MG5_aMC_v3_2_0
chmod +x makeDYTOYLHEFs.sh
./makeDYTOYLHEFs.sh
cd ..

# Construction of toy calculations

cd dire
chmod +x makeToyInc.sh
chmod +x makeToyExc.sh

# Create inclusive toy calculations
# Arguments: final-state parton multiplicity, #events, random seed
./makeToyInc.sh 3 1000 1234
./makeToyInc.sh 2 1000 1234
./makeToyInc.sh 1 1000 1234
./makeToyInc.sh 0 1000 1234

# Create exclusive toy calculations
# Arguments: final-state parton multiplicity, #events, random seed
./makeToyExc.sh 2 1000 1234
./makeToyExc.sh 1 1000 1234
./makeToyExc.sh 0 1000 1234

cd ..

# Production of Tomte N3LO+PS matched predictions

cd dire
chmod +x runDy.sh
# Arguments: #events, random seed
./runDy.sh 1000 1234
cd ..

# Postprocessing/plotting:
# The output of Tomte is a Les Houches Event file containing the result
# of the calculations in the form of weighted events. These may be histogrammed
# with an external tool (such as Rivet). 

