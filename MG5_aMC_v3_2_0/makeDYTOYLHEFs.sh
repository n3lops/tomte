
# Note: On Suse/Ubuntu, experienced unclear problems
# when running under Python 3. Alternatively, could use
# a systemwide Python 2.7 installation here...

# Use the LHAPDF version installed previously to set PDFs for MG5
lhapdf=$(find ../ -name 'lhapdf-config' | head -n 1 | sed "s,\.\.,$(pwd)/\.\.,g")

cp input/mg5_configuration.txt input/mg5_configuration.txt.orig
sed -i "s,= <lhapdf-config>,= $lhapdf,g" input/mg5_configuration.txt


for name in {0g,1g,2g,3g} ; do

rm -rf dd2ee_$name
#/usr/bin/python2.7 ./bin/mg5_aMC procs_dd2ee${name}.dat
./bin/mg5_aMC procs_dd2ee${name}.dat

count=100;
while [[ $count -lt 101 ]] ; do
let count++
runname=dy$name$count
cp runcard_pp2eeng.dat.template runcard.tmp
sed "s/<seed>/$count/g" -i runcard.tmp
sed "s/<runname>/$runname/g" -i runcard.tmp
cp runcard.tmp dd2ee_$name/Cards/run_card.dat
cd dd2ee_$name
#/usr/bin/python2.7 ./bin/generate_events 2 10 $runname
./bin/generate_events 2 10 $runname
cd ..
done

done

mv input/mg5_configuration.txt.orig input/mg5_configuration.txt
