import ruamel.yaml as yaml
import numpy as np
import matplotlib.pyplot as plt
import MatplotlibSettings
from scipy.interpolate import make_interp_spline, BSpline

# Loada data
data = np.loadtxt("PDFExpansion.dat")

muv = [10, 80, 91.188, 100, 1000]
ndt = 1000

enmap = {0: 0, 3: 1, 4: 2}
Qmap = {0: "10", 3: "100", 4: "1000"}

f, ax = plt.subplots(3, 1, sharex = "all", gridspec_kw = dict(width_ratios = [1], height_ratios = [1, 1, 1]))
plt.subplots_adjust(wspace = 0, hspace = 0.05)

for i in [0, 3, 4]:
    x       = data[i*ndt:(i+1)*ndt,3]
    explo   = data[i*ndt:(i+1)*ndt,4]
    expnlo  = data[i*ndt:(i+1)*ndt,5]
    expnnlo = data[i*ndt:(i+1)*ndt,6]
    res     = data[i*ndt:(i+1)*ndt,7]

    if i == 0:
        ax[enmap[i]].set_title(r"\textbf{Down PDF, $\mu_{\rm R}=\mu_{\rm F}=M_Z$", fontsize = 20)

    ax[enmap[i]].set_xlim([0.0001, 1])
    ax[enmap[i]].set_ylim([0.7, 1.3])
    ax[enmap[i]].set_xscale("log")
    ax[enmap[i]].text(0.00012, 0.75, r"\textbf{$Q$ = " + Qmap[i] + " GeV}", fontsize = 18)
    if i == 3:
        ax[enmap[i]].set_ylabel(r"\textbf{Ratio to Resummed}", fontsize = 20)
    if i == 4:
        ax[enmap[i]].set_xlabel(r"\textbf{$x$}")
    ax[enmap[i]].set_ylim([0.7, 1.3])
    ax[enmap[i]].plot(x, res/res,     label = r"\textbf{Resummed}", color = "black", lw = 1.5, ls = "--")
    ax[enmap[i]].plot(x, explo/res,   label = r"\textbf{LO expansion}",      color = "red", lw = 1.8)
    ax[enmap[i]].plot(x, expnlo/res,  label = r"\textbf{NLO expansion}",     color = "blue", lw = 1.8)
    ax[enmap[i]].plot(x, expnnlo/res, label = r"\textbf{NNLO expansion}",    color = "green", lw = 1.8)
    if i == 3:
        ax[enmap[i]].legend(ncol = 2, fontsize = 16, loc = "upper right")

plt.savefig("PDFExpansionJoint.pdf")
plt.close()

