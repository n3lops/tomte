var searchData=
[
  ['lo_20evolution_20kernels_0',['LO evolution kernels',['../group__LOunpevk.html',1,'']]],
  ['lo_20massive_20coefficient_20functions_1',['LO massive coefficient functions',['../group__LO.html',1,'']]],
  ['lo_20massive_2dzero_20coefficient_20functions_2',['LO massive-zero coefficient functions',['../group__LOZero.html',1,'']]],
  ['lo_20splitting_20functions_3',['LO splitting functions',['../group__LOpolsf.html',1,'(Global Namespace)'],['../group__LOtranssf.html',1,'(Global Namespace)'],['../group__LOunpsf.html',1,'(Global Namespace)'],['../group__LOunpsftl.html',1,'(Global Namespace)']]],
  ['longitudinally_20polarised_20splitting_20functions_4',['Longitudinally polarised splitting functions',['../group__PolSF.html',1,'']]]
];
