var searchData=
[
  ['matchedevolution_0',['MatchedEvolution',['../classapfel_1_1MatchedEvolution.html#adb2660fb7842ff7cfd1babfcdb2b0a05',1,'apfel::MatchedEvolution::MatchedEvolution()=delete'],['../classapfel_1_1MatchedEvolution.html#aebbd27d9fa7dc916378a4b4d726b5d26',1,'apfel::MatchedEvolution::MatchedEvolution(T const &amp;ObjRef, double const &amp;MuRef, std::vector&lt; double &gt; const &amp;Thresholds, int const &amp;nsteps=10)']]],
  ['matchingbasisqcd_1',['MatchingBasisQCD',['../classapfel_1_1MatchingBasisQCD.html#a9dc8f980a586c204b52dfff6626b78ae',1,'apfel::MatchingBasisQCD']]],
  ['matchingfunctionsffs_2',['MatchingFunctionsFFs',['../namespaceapfel.html#ad153699746a0c4dd12021c0bb949fad5',1,'apfel']]],
  ['matchingfunctionspdfs_3',['MatchingFunctionsPDFs',['../namespaceapfel.html#a05c2fea27dd7ef345b9da22ce8a63a5d',1,'apfel']]],
  ['matchingoperatorbasisqcd_4',['MatchingOperatorBasisQCD',['../classapfel_1_1MatchingOperatorBasisQCD.html#a891d84b83ee0ecc589a5f3833e54852f',1,'apfel::MatchingOperatorBasisQCD']]],
  ['matchobject_5',['MatchObject',['../classapfel_1_1AlphaQCD.html#a2d1c556fa5a4173f3577234e4615192e',1,'apfel::AlphaQCD::MatchObject()'],['../classapfel_1_1AlphaQED.html#a27224bd5df84b04b6334e61b6a370133',1,'apfel::AlphaQED::MatchObject()'],['../classapfel_1_1Dglap.html#a2ba5c1a9fca42d1cae5e369193e28c47',1,'apfel::Dglap::MatchObject()'],['../classapfel_1_1MatchedEvolution.html#a82f4e0ff82cd9b5f1e393625b4d81bc4',1,'apfel::MatchedEvolution::MatchObject()']]],
  ['matchtmdffs_6',['MatchTmdFFs',['../namespaceapfel.html#a439e7dcdb08932f7a2ca78cffda1af00',1,'apfel']]],
  ['matchtmdjet_7',['MatchTmdJet',['../namespaceapfel.html#aa007f4e8cdb02b3c2117f6bfedf3b67a',1,'apfel']]],
  ['matchtmdpdfs_8',['MatchTmdPDFs',['../namespaceapfel.html#a49344b8e01187276f0b247d170bc3b26',1,'apfel']]],
  ['matrix_9',['matrix',['../classapfel_1_1matrix.html#ad233ffd5afb1d9ec9ba46a39202338b1',1,'apfel::matrix']]],
  ['max_10',['max',['../classapfel_1_1ExtendedVector.html#a3535622f96f490c0192557945e02daf6',1,'apfel::ExtendedVector']]],
  ['method_11',['Method',['../classapfel_1_1Integrator.html#ad43e4272eef0c1dbdac807aee547ec17',1,'apfel::Integrator']]],
  ['min_12',['min',['../classapfel_1_1ExtendedVector.html#af591d865e71439884f65bf0b13007dce',1,'apfel::ExtendedVector']]],
  ['multiplyby_13',['MultiplyBy',['../classapfel_1_1DoubleObject.html#a0733abc771d5519e246f6a318c868edf',1,'apfel::DoubleObject']]]
];
