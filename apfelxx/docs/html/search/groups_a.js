var searchData=
[
  ['time_2dlike_20matching_20conditions_0',['Time-like matching conditions',['../group__MatchCondTL.html',1,'']]],
  ['time_2dlike_20matching_20functions_1',['Time-like matching functions',['../group__TLMatchFunc.html',1,'']]],
  ['time_2dlike_20splitting_20functions_2',['Time-like splitting functions',['../group__TLSplittings.html',1,'']]],
  ['tmd_20matching_20functions_3',['TMD matching functions',['../group__TMDMatchingFunctions.html',1,'']]],
  ['transversely_20polarised_20splitting_20functions_4',['Transversely polarised splitting functions',['../group__TransSF.html',1,'(Global Namespace)'],['../group__TransSFtl.html',1,'(Global Namespace)']]]
];
