var searchData=
[
  ['masses_0',['Masses',['../structapfel_1_1EvolutionSetup.html#a4020afe682b670da3163f676775a67cb',1,'apfel::EvolutionSetup']]],
  ['massrenscheme_1',['MassRenScheme',['../structapfel_1_1EvolutionSetup.html#a88411dbe8022d8cdc9b4bace53949cd9',1,'apfel::EvolutionSetup']]],
  ['matchingconditions_2',['MatchingConditions',['../structapfel_1_1DglapObjects.html#a7cdc6caad7aa2128f23bd467f109cb40',1,'apfel::DglapObjects']]],
  ['matchingfunctionsffs_3',['MatchingFunctionsFFs',['../structapfel_1_1TmdObjects.html#a5167975f18155c85fe6b2e506e096394',1,'apfel::TmdObjects']]],
  ['matchingfunctionspdfs_4',['MatchingFunctionsPDFs',['../structapfel_1_1TmdObjects.html#a8e043aa1d00e699e6ca2e320422b0684',1,'apfel::TmdObjects']]]
];
