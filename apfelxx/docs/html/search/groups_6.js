var searchData=
[
  ['nlo_20massive_20coefficient_20functions_0',['NLO massive coefficient functions',['../group__NLO.html',1,'']]],
  ['nlo_20massive_2dzero_20coefficient_20functions_1',['NLO massive-zero coefficient functions',['../group__NLOZero.html',1,'']]],
  ['nlo_20matching_20conditions_2',['NLO matching conditions',['../group__NLOMC.html',1,'']]],
  ['nlo_20matching_20functions_20for_20boer_2dmulders_20pdfs_3',['NLO matching functions for Boer-Mulders PDFs',['../group__NLOBM.html',1,'']]],
  ['nlo_20matching_20functions_20for_20ffs_4',['NLO matching functions for FFs',['../group__NLOff.html',1,'']]],
  ['nlo_20matching_20functions_20for_20pdfs_5',['NLO matching functions for PDFs',['../group__NLOmatch.html',1,'']]],
  ['nlo_20splitting_20functions_6',['NLO splitting functions',['../group__NLOpolsf.html',1,'(Global Namespace)'],['../group__NLOtranssf.html',1,'(Global Namespace)'],['../group__NLOunpsf.html',1,'(Global Namespace)'],['../group__NLOunpsftl.html',1,'(Global Namespace)']]],
  ['nlo_20zero_2dmass_20coefficient_20functions_7',['NLO zero-mass coefficient functions',['../group__NLOzm.html',1,'(Global Namespace)'],['../group__NLOzmpol.html',1,'(Global Namespace)'],['../group__NLOzmSIA.html',1,'(Global Namespace)']]],
  ['nnlo_20matching_20conditions_8',['NNLO matching conditions',['../group__NNLOMC.html',1,'']]],
  ['nnlo_20matching_20functions_20for_20boer_2dmulders_20pdfs_9',['NNLO matching functions for Boer-Mulders PDFs',['../group__NNLOBM.html',1,'']]],
  ['nnlo_20matching_20functions_20for_20ffs_10',['NNLO matching functions for FFs',['../group__NNLOff.html',1,'']]],
  ['nnlo_20matching_20functions_20for_20pdfs_11',['NNLO matching functions for PDFs',['../group__NNLOmatch.html',1,'']]],
  ['nnlo_20splitting_20functions_12',['NNLO splitting functions',['../group__NNLOpolsf.html',1,'(Global Namespace)'],['../group__NNLOunpsf.html',1,'(Global Namespace)'],['../group__NNLOunpsftl.html',1,'(Global Namespace)']]],
  ['nnlo_20zero_2dmass_20coefficient_20functions_13',['NNLO zero-mass coefficient functions',['../group__NNLOzm.html',1,'(Global Namespace)'],['../group__NNLOzmSIA.html',1,'(Global Namespace)']]],
  ['nnnlo_20matching_20functions_20for_20pdfs_14',['NNNLO matching functions for PDFs',['../group__NNNLOmatch.html',1,'']]],
  ['nnnlo_20splitting_20functions_15',['NNNLO splitting functions',['../group__NNNLOunpsf.html',1,'']]],
  ['numerical_20constants_16',['Numerical constants',['../group__NumericalConstants.html',1,'']]]
];
